package com.limelitesolutions.tnplite;

import android.content.SearchRecentSuggestionsProvider;

public class RecentSearchesProviderSearchBar extends SearchRecentSuggestionsProvider {
	
	public static final String AUTHORITY = RecentSearchesProviderSearchBar.class.getName();
	public static final int MODE = DATABASE_MODE_QUERIES;
	
	public RecentSearchesProviderSearchBar() {
		setupSuggestions(AUTHORITY,MODE);
	}

}


