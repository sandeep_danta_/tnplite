package com.limelitesolutions.tnplite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Window;

public class SyncInProgress extends SherlockActivity {

	URI tnpMsgURI;
	String URIString, attachLink;
	String setter = "";
	HttpResponse htmlview;
	TextView msgviewer, titleviewer, dateviewer;
	ProgressDialog waitProgress;

	public void onCreate(Bundle savedInstanceState) {

		waitProgress = new ProgressDialog(SyncInProgress.this);

		waitProgress.setTitle("Loading");
		waitProgress.setMessage("Please wait...");
		waitProgress.show();

		super.onCreate(savedInstanceState);
		SyncInProgress.this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.sync_in_progress);

		msgviewer = (TextView) findViewById(R.id.notification_msg);
		titleviewer = (TextView) findViewById(R.id.notification_title);
		dateviewer = (TextView) findViewById(R.id.notification_date);

		URIString = TNPListView.clicklink;
		// if (URIString != null & !URIString.contentEquals("")) {
		msgDisplayer fullMessage = new msgDisplayer();
		fullMessage.execute(URIString);
		// }

	}

	public class msgDisplayer extends AsyncTask<String, Void, String> {

		protected String doInBackground(String... params) {
			HttpClient tempClient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(tempClient.getParams(),
					30000);

			try {
				// String uriInput = params[0];

				// if (uriInput != null & !uriInput.contentEquals("")) {
				tnpMsgURI = new URI(params[0]);

				HttpGet getter = new HttpGet(tnpMsgURI);

				htmlview = tempClient.execute(getter);

				if (htmlview != null) {

					InputStream in;

					in = htmlview.getEntity().getContent();
					InputStreamReader inread = new InputStreamReader(in,
							"windows-1252");
					BufferedReader buff = new BufferedReader(inread);

					StringBuilder sb = new StringBuilder();
					String readline;
					while ((readline = buff.readLine()) != null) {
						sb.append(readline + "\n");
					}

					setter = sb.toString();
					Document setterParsing = Jsoup
							.parse(setter, "windows-1252");

					Elements tpUpdates = setterParsing.getElementsByTag("div");
					setter = tpUpdates.html();

					setter = setter.replace("<br />", "");
					setter = setter.replace("<p></p>", "\n");
					setter = setter.replace("&amp;", "&");
					setter = setter.replace("&gt;", ">");
					setter = setter.replace("&lt;", "<");
					setter = setter.replace("�", "-");
					setter = setter.replace(" .", ".");
					setter = setter.replace(" ,", ",");
					setter = setter.replace("&quot;", "\"");
					setter = setter.replace("\n\n\n", "\n\n");

					Elements attachments = setterParsing.getElementsByTag("A");

					String attachLinkPart = attachments.attr("HREF");
					if (!attachLinkPart.contentEquals("")) {
						attachLink = "http://tp.iitkgp.ernet.in/notice/"
								+ attachLinkPart;
						setter = setter + "\n" + "Attachment: " + attachLink
								+ "\n";
					}
				}
				// } else {
				// setter = "Error Sending Request. Please try again by "
				// + "pressing BACK and accessing again."
				// + " If the error persists, please restart the app";
				// }
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				setter = "Error Sending Request. Please try again by "
						+ "pressing BACK and accessing again."
						+ " If the error persists, please restart the app";

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				setter = "Error Sending Request. Please try again by "
						+ "pressing BACK and accessing again."
						+ " If the error persists, please restart the app";

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				setter = "Error Sending Request. Please try again by "
						+ "pressing BACK and accessing again."
						+ " If the error persists, please restart the app";

			} catch (Exception e) {
				e.printStackTrace();
				setter = "Error Sending Request. Please try again by "
						+ "pressing BACK and accessing again."
						+ " If the error persists, please restart the app";

			}
			return setter;
		}

		public void onPostExecute(String result) {
			waitProgress.dismiss();
			msgviewer.setText(result);
			titleviewer.setText(TNPListView.postTitle);
			dateviewer.setText(TNPListView.postDate);
			msgviewer.setTypeface(Typeface.createFromAsset(getAssets(),
					"Fonts/Helvetica.ttf"));
			titleviewer.setTypeface(Typeface.createFromAsset(getAssets(),
					"Fonts/Helvetica-Bold.otf"));
			dateviewer.setTypeface(Typeface.createFromAsset(getAssets(),
					"Fonts/Helvetica.ttf"));
		}

	}
}
