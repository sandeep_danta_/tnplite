package com.limelitesolutions.tnplite;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import android.os.AsyncTask;



public class CheckInternetConnection extends AsyncTask<Void, Void, Boolean> {

	URI tnperr;
	HttpResponse getConnection;
	Boolean isConnected;

	protected Boolean doInBackground(Void... arg0) {

		HttpClient tempClient = new DefaultHttpClient();
		HttpConnectionParams
				.setConnectionTimeout(tempClient.getParams(), 10000);
		try {
			tnperr = new URI("http://tp.iitkgp.ernet.in");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		HttpGet getter = new HttpGet(tnperr);
		

		try {
			getConnection = tempClient.execute(getter);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (getConnection != null) {
			isConnected = true;
		} else {
			isConnected = false;
		}
		return isConnected;
	}

}
