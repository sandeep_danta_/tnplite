package com.limelitesolutions.tnplite;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("DefaultLocale")
public class TnPListviewAdapter extends ArrayAdapter<Notice> implements
		Filterable {

	public boolean isFilteringComplete = true;

	private List<Notice> tnpPostList, filteredTnPpostList;
	private Context tnpContext;
	private Filter postFilter;

	private static class NoticeHolder {

		public TextView dateTimeHolder;
		public TextView messageHolder;
	}

	public TnPListviewAdapter(Context tnpContext,
			List<Notice> filteredTnPpostList) {

		super(tnpContext, R.layout.tnplistview_listitem, filteredTnPpostList);
		this.tnpContext = tnpContext;
		this.tnpPostList = filteredTnPpostList;
		this.filteredTnPpostList = filteredTnPpostList;

	}

	public int getCount() {
		return filteredTnPpostList.size();
	}

	public Notice getItem(int position) {
		return filteredTnPpostList.get(position);
	}

	public void resetFilter() {
		filteredTnPpostList = tnpPostList;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		NoticeHolder holder = new NoticeHolder();

		if (convertView == null) {

			LayoutInflater postInflater = (LayoutInflater) tnpContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = postInflater.inflate(R.layout.tnplistview_listitem, parent,
					false);

			// TextView listPostNumber = (TextView) v
			// .findViewById(R.id.listItem_postNumber);
			// TextView listAnyAttachments = (TextView) v
			// .findViewById(R.id.listItem_anyAttachments);
			TextView listDateTime = (TextView) v
					.findViewById(R.id.listItem_dateTime);
			TextView listMessage = (TextView) v
					.findViewById(R.id.listItem_message);

			listMessage.setTypeface(Typeface.createFromAsset(v.getContext()
					.getAssets(), "Fonts/Helvetica.ttf"));

			// holder.postNumberHolder = listPostNumber;
			// holder.anyAttachmentHolder = listAnyAttachments;
			holder.dateTimeHolder = listDateTime;
			holder.messageHolder = listMessage;

			v.setTag(holder);

		} else {

			holder = (NoticeHolder) v.getTag();

		}

		Notice singlePost = filteredTnPpostList.get(position);

		ImageView listImageAttachements = (ImageView) v
				.findViewById(R.id.listItem_anyAttachmentsImage);

		// holder.postNumberHolder.setText(singlePost.getPostNumber());
		// holder.anyAttachmentHolder.setText(singlePost.getAnyAttachments());
		if (singlePost.getAnyAttachments().contentEquals("A")) {
			listImageAttachements.setVisibility(View.VISIBLE);
		} else {
			listImageAttachements.setVisibility(View.INVISIBLE);
		}
		holder.dateTimeHolder.setText(singlePost.getPostDateTime());
		holder.messageHolder.setText(singlePost.getPostMessage());

		return v;

	}

	public Filter getFilter() {

		if (postFilter == null) {
			postFilter = new NoticeFilter();
		}

		return postFilter;
	}

	@SuppressLint("DefaultLocale")
	private class NoticeFilter extends Filter {

		@SuppressLint("DefaultLocale")
		protected FilterResults performFiltering(CharSequence arg0) {

			isFilteringComplete = false;

			FilterResults filteredNotices = new FilterResults();

			if (arg0 == null || arg0.length() == 0) {
				filteredNotices.values = tnpPostList;
				filteredNotices.count = tnpPostList.size();
				isFilteringComplete = true;
			} else {
				List<Notice> fPostList = new ArrayList<Notice>();

				for (Notice oneNotice : tnpPostList) {
					if (oneNotice.postMessage.toUpperCase().contains(
							arg0.toString().toUpperCase())
							|| oneNotice.postDateTime.toUpperCase().contains(
									arg0.toString().toUpperCase())
							|| oneNotice.postNumber.toUpperCase().contains(
									arg0.toString().toUpperCase())) {

						fPostList.add(oneNotice);

					}
				}

				filteredNotices.values = fPostList;
				filteredNotices.count = fPostList.size();
				isFilteringComplete = true;

			}

			return filteredNotices;

		}

		@SuppressWarnings("unchecked")
		protected void publishResults(CharSequence arg0, FilterResults arg1) {

			if (!arg0.toString().contentEquals("")) {

				if (arg1.count == 0) {
					filteredTnPpostList = (List<Notice>) arg1.values;
					// filteredTnPpostList = tnpPostList;
					notifyDataSetChanged();

				} else {
					filteredTnPpostList = (List<Notice>) arg1.values;
					notifyDataSetChanged();
				}

			} else {
				filteredTnPpostList = tnpPostList;
				notifyDataSetChanged();
				isFilteringComplete = true;
			}

		}

	}

}
