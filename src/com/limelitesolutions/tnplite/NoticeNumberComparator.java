package com.limelitesolutions.tnplite;

import java.util.Comparator;

public class NoticeNumberComparator implements Comparator<Notice> {

	@Override
	public int compare(Notice lhs, Notice rhs) {
		return Integer.parseInt(rhs.getPostNumber()) - Integer.parseInt(lhs.getPostNumber());
	}

}
