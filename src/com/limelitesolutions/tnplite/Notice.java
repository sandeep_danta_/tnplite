package com.limelitesolutions.tnplite;

public class Notice {

	public String postNumber;
	public String anyAttachments;
	public String postDateTime;
	public String postMessage;

	public Notice(String postNumber, String anyAttachments,
			String postDateTime, String postMessage) {
		this.postNumber = postNumber;
		this.anyAttachments = anyAttachments;
		this.postDateTime = postDateTime;
		this.postMessage = postMessage;
	}

	public Notice() {
	}

	public void setPostNumber(String postNumber) {
		this.postNumber = postNumber;
	}

	public void setAnyAttachments(String anyAttachments) {
		this.anyAttachments = anyAttachments;
	}

	public void setPostDateTime(String postDateTime) {
		this.postDateTime = postDateTime;
	}

	public void setPostMessage(String postMessage) {
		this.postMessage = postMessage;
	}

	public String getPostNumber() {
		return postNumber;
	}

	public String getAnyAttachments() {
		return anyAttachments;
	}

	public String getPostDateTime() {
		return postDateTime;
	}

	public String getPostMessage() {
		return postMessage;
	}

}
