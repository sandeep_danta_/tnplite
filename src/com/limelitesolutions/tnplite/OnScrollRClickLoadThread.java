package com.limelitesolutions.tnplite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class OnScrollRClickLoadThread implements Runnable {

	int pageNumber;
	String uriString;

	String defaultTNPPageLink = "http://tp.iitkgp.ernet.in/notice/";

	StringBuilder htmlBuild = new StringBuilder();
	StringBuilder dateBuild = new StringBuilder();
	StringBuilder linkBuild = new StringBuilder();
	StringBuilder msgBuild = new StringBuilder();
	StringBuilder attachBuild = new StringBuilder();

	String[] dateArray, linkArray, msgArray, attachArray;

	ArrayList<Notice> tnpNoticeList = new ArrayList<Notice>();

	public OnScrollRClickLoadThread(int pageNumber) {

		this.pageNumber = pageNumber;

	}

	public ArrayList<Notice> getNotices() {

		return tnpNoticeList;

	}

	@Override
	public void run() {

		uriString = defaultTNPPageLink + "index.php?page=" + pageNumber;

		try {
			URI listGetURI = new URI(uriString);

			HttpClient tnpClient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(tnpClient.getParams(),
					10000);

			HttpGet sendTNPRequest = new HttpGet(listGetURI);
			HttpResponse getTNPList = tnpClient.execute(sendTNPRequest);

			if (getTNPList != null) {
				BufferedReader tnpClientBufferRead = new BufferedReader(
						new InputStreamReader(getTNPList.getEntity()
								.getContent()));

				String readHTMLline;

				while ((readHTMLline = tnpClientBufferRead.readLine()) != null) {
					htmlBuild.append(readHTMLline + "\n");
				}

				Document htmlBuildDoc = Jsoup.parse(htmlBuild.toString());
				Elements tnpListElements = htmlBuildDoc.getElementsByTag("td");
				Elements tnpLinkElements = htmlBuildDoc.getElementsByTag("a");

				for (Element eachEle : tnpListElements) {
					String widthAttribute = eachEle.attr("width");
					String widtAttribute = eachEle.attr("widt");

					if (widthAttribute.contentEquals("5%")
							|| widtAttribute.contentEquals("5%")) {

						if (eachEle.text().contentEquals("A")) {

							attachBuild.append("A" + "\n");
						} else {
							attachBuild.append(" " + "\n");
						}

					} else if (widthAttribute.contentEquals("25%")) {

						dateBuild.append(eachEle.text() + "\n");

					} else if (widthAttribute.contentEquals("70%")) {
						msgBuild.append(eachEle.text() + "\n");
					}

				}

				for (Element eachEle : tnpLinkElements) {
					linkBuild.append(eachEle.attr("href").replace(
							"notice.php?sr_no=", "")
							+ "\n");
				}

				dateArray = dateBuild.toString().split("\n");
				attachArray = attachBuild.toString().split("\n");
				linkArray = linkBuild.toString().split("\n");
				msgArray = msgBuild.toString().split("\n");

				int dateArrayLength = dateArray.length;

				for (int i = 0; i < dateArrayLength; i++) {
					Notice notice = new Notice();
					notice.postNumber = linkArray[i];
					notice.anyAttachments = attachArray[i];
					notice.postDateTime = dateArray[i];
					notice.postMessage = msgArray[i];
					tnpNoticeList.add(notice);
				}
			}

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
