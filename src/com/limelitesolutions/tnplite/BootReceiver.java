package com.limelitesolutions.tnplite;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

	public void onReceive(Context arg0, Intent arg1) {

		if ("android.intent.action.BOOT_COMPLETED".equals(arg1.getAction())) {

			System.out.println("Boot Received puahahaha");
			AlarmManager am = (AlarmManager) arg0
					.getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(arg0, TnPNotifier.class);
			arg0.startService(i);
			PendingIntent pi = PendingIntent.getService(arg0, 0, i, 0);
			am.cancel(pi);

			am.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
					300000, 30000, pi);
		}

	}

}