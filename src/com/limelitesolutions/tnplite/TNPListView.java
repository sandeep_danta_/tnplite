package com.limelitesolutions.tnplite;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.actionbarsherlock.widget.SearchView;

public class TNPListView extends SherlockActivity {

	int pgNo, y, threadLooper;
	boolean isLoadingOnClick, isLoadingOnScroll, isDatasetModifying,
			isConnectedtoInternet;
	List<Notice> noticeList = new ArrayList<Notice>();

	String noticePgURIString;
	String defaultTNPPageLink = "http://tp.iitkgp.ernet.in/notice/";

	ListView tnpListViewPage;
	SearchView searchBar;

	ProgressDialog displayLoading;
	View tnpListFooterLoading;

	String searchQuery = "";

	TextView viewMore;
	ProgressBar progressBar;

	SearchRecentSuggestions recentSearches = new SearchRecentSuggestions(
			TNPListView.this, RecentSearchesProviderSearchBar.AUTHORITY,
			RecentSearchesProviderSearchBar.MODE);

	static String clicklink, postTitle, postDate;

	TnPListviewAdapter tnpListadapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tnplistview_activity);

		tnpListViewPage = (ListView) findViewById(R.id.tnplistview);
		tnpListViewPage.setTextFilterEnabled(true);

		setNotificationAlarm();

		isLoadingOnClick = false;
		isLoadingOnScroll = false;

		startSession();

		tnpListViewPage.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				if (arg2 == arg0.getCount() - 1) {
					if (tnpListFooterLoading
							.findViewById(R.id.loading_progress)
							.getVisibility() == View.INVISIBLE) {

						CheckInternetConnection checkConnection = new CheckInternetConnection();
						try {
							isConnectedtoInternet = checkConnection.execute()
									.get();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						} catch (ExecutionException e1) {
							e1.printStackTrace();
						}

						if (tnpListadapter.isFilteringComplete) {

							if (isConnectedtoInternet) {

								tnpListFooterLoading.findViewById(
										R.id.loading_progress).setVisibility(
										View.VISIBLE);
								tnpListFooterLoading.findViewById(
										R.id.footerListView).setVisibility(
										View.INVISIBLE);

								y = 0;
								for (threadLooper = 0; threadLooper < 10; threadLooper++) {

									pgNo++;

									String onClickNoticePgURIString = defaultTNPPageLink
											+ "index.php?page=" + pgNo;
									TNPListGetOnScroll pingz = new TNPListGetOnScroll();
									pingz.execute(onClickNoticePgURIString);
								}

							} else {
								Toast.makeText(TNPListView.this,
										"Connection Lost", Toast.LENGTH_SHORT)
										.show();
							}

						}
					}

				} else {
					clicklink = "http://tp.iitkgp.ernet.in/notice/notice.php?sr_no="
							+ tnpListadapter.getItem(arg2).postNumber;
					postTitle = tnpListadapter.getItem(arg2).postMessage;
					postDate = tnpListadapter.getItem(arg2).postDateTime;
					Intent viewDetailedMessage = new Intent(
							"com.limelitesolutions.tnplite.SYNCINPROGRESS");
					startActivity(viewDetailedMessage);
				}
			}

		});

	}

	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchBar = (SearchView) menu.findItem(
				R.id.menu_search).getActionView();

		if (searchBar != null) {

			searchBar.setSearchableInfo(searchManager
					.getSearchableInfo(getComponentName()));
			searchBar.setIconifiedByDefault(true);
			searchBar.setQueryHint("Search NoticeBoard");
		}

		SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {

			public boolean onQueryTextSubmit(String query) {

				searchQuery = query;
				if (query.toString().contentEquals("")) {

					tnpListadapter.resetFilter();

				}

				tnpListadapter.getFilter().filter(query);

				//recentSearches.saveRecentQuery(query, null);
				searchBar.clearFocus();
				return true;
			}

			public boolean onQueryTextChange(String newText) {

				searchQuery = newText;
				if (newText.toString().contentEquals("")) {

					tnpListadapter.resetFilter();

				}

				tnpListadapter.getFilter().filter(newText);

				//recentSearches.saveRecentQuery(newText, null);

				return true;
			}

		};

		searchBar.setOnQueryTextListener(queryTextListener);

		MenuItem menuSearch = menu.findItem(R.id.menu_search);

		menuSearch.setOnActionExpandListener(new OnActionExpandListener() {

			public boolean onMenuItemActionExpand(MenuItem item) {
				return true;
			}

			public boolean onMenuItemActionCollapse(MenuItem item) {
				searchQuery = "";
				tnpListViewPage.setAdapter(tnpListadapter);
				tnpListadapter.getFilter().filter(searchQuery);
				tnpListViewPage.smoothScrollToPosition(-10);
				return true;
			}
		});

		MenuItem menuHome = menu.findItem(R.id.menu_home);

		// Capture menu item clicks
		menuHome.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				tnpListViewPage.setAdapter(tnpListadapter);
				tnpListadapter.getFilter().filter(searchQuery);
				tnpListViewPage.smoothScrollToPosition(-10);

				return false;
			}

		});

		return true;
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

	}

	private class TNPListGetter extends
			AsyncTask<String, Void, ArrayList<Notice>> {

		StringBuilder htmlBuild = new StringBuilder();
		StringBuilder dateBuild = new StringBuilder();
		StringBuilder linkBuild = new StringBuilder();
		StringBuilder msgBuild = new StringBuilder();
		StringBuilder attachBuild = new StringBuilder();

		String[] dateArray, linkArray, msgArray, attachArray;

		ArrayList<Notice> tnpNoticeList = new ArrayList<Notice>();

		protected ArrayList<Notice> doInBackground(String... arg0) {

			try {

				URI listGetURI = new URI(arg0[0]);

				HttpClient tnpClient = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(
						tnpClient.getParams(), 30000);

				HttpGet sendTNPRequest = new HttpGet(listGetURI);
				HttpResponse getTNPList = tnpClient.execute(sendTNPRequest);

				if (getTNPList != null) {
					BufferedReader tnpClientBufferRead = new BufferedReader(
							new InputStreamReader(getTNPList.getEntity()
									.getContent(), "windows-1252"));

					String readHTMLline;

					while ((readHTMLline = tnpClientBufferRead.readLine()) != null) {
						htmlBuild.append(readHTMLline + "\n");
					}

					Document htmlBuildDoc = Jsoup.parse(htmlBuild.toString(),
							"windows-1252");
					Elements tnpListElements = htmlBuildDoc
							.getElementsByTag("td");
					Elements tnpLinkElements = htmlBuildDoc
							.getElementsByTag("a");

					for (Element eachEle : tnpListElements) {
						String widthAttribute = eachEle.attr("width");
						String widtAttribute = eachEle.attr("widt");

						if (widthAttribute.contentEquals("5%")
								|| widtAttribute.contentEquals("5%")) {

							if (eachEle.text().contentEquals("A")) {

								attachBuild.append("A" + "\n");
							} else {
								attachBuild.append(" " + "\n");
							}

						} else if (widthAttribute.contentEquals("25%")) {

							dateBuild.append(eachEle.text() + "\n");

						} else if (widthAttribute.contentEquals("70%")) {
							msgBuild.append(eachEle.text() + "\n");
						}

					}

					for (Element eachEle : tnpLinkElements) {
						linkBuild.append(eachEle.attr("href").replace(
								"notice.php?sr_no=", "")
								+ "\n");
					}

					dateArray = dateBuild.toString().split("\n");
					attachArray = attachBuild.toString().split("\n");
					linkArray = linkBuild.toString().split("\n");
					msgArray = msgBuild.toString().split("\n");

					int dateArrayLength = dateArray.length;
					int msgArrayLength = msgArray.length;

					if (msgArrayLength != dateArrayLength) {
						TNPListView.this.finish();
					} else {

						for (int i = 0; i < dateArrayLength; i++) {
							Notice notice = new Notice();
							notice.postNumber = linkArray[i];
							notice.anyAttachments = attachArray[i];
							notice.postDateTime = dateArray[i];
							notice.postMessage = msgArray[i];
							tnpNoticeList.add(notice);
						}
					}

				}

			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return tnpNoticeList;

		}

		protected void onPostExecute(ArrayList<Notice> results) {

			noticeList = results;

			tnpListadapter = new TnPListviewAdapter(TNPListView.this,
					noticeList);
			tnpListViewPage.setAdapter(tnpListadapter);

			isDatasetModifying = false;
			String fileName = "lastNotif";
			FileOutputStream fileNameStream;

			try {
				fileNameStream = openFileOutput(fileName, Context.MODE_PRIVATE);
				fileNameStream.write(results.get(0).postNumber.getBytes());
				fileNameStream.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			displayLoading.dismiss();

		}
	}

	private class ContinousScrollListener implements OnScrollListener {

		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

		}

		public void onScrollStateChanged(AbsListView view, int scrollState) {

			if (tnpListFooterLoading.findViewById(R.id.loading_progress)
					.getVisibility() == View.INVISIBLE
					&& tnpListadapter.isFilteringComplete) {
				if (scrollState == SCROLL_STATE_IDLE) {
					if (view.getLastVisiblePosition() >= view.getCount() - 1) {
						isLoadingOnScroll = true;

						CheckInternetConnection checkConnection = new CheckInternetConnection();
						try {
							isConnectedtoInternet = checkConnection.execute()
									.get();
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						} catch (ExecutionException e1) {
							e1.printStackTrace();
						}

						if (isConnectedtoInternet) {

							tnpListFooterLoading.findViewById(
									R.id.loading_progress).setVisibility(
									View.VISIBLE);
							tnpListFooterLoading.findViewById(
									R.id.footerListView).setVisibility(
									View.INVISIBLE);

							y = 0;
							for (threadLooper = 0; threadLooper < 10; threadLooper++) {

								pgNo++;

								String onClickNoticePgURIString = defaultTNPPageLink
										+ "index.php?page=" + pgNo;
								TNPListGetOnScroll pingz = new TNPListGetOnScroll();
								pingz.execute(onClickNoticePgURIString);

							}

							tnpListadapter.getFilter().filter(searchQuery);
						} else {
							Toast.makeText(TNPListView.this, "Connection Lost",
									Toast.LENGTH_SHORT).show();
						}
					}
				}
			}
		}

	}

	private class TNPListGetOnScroll extends
			AsyncTask<String, Void, ArrayList<Notice>> {

		StringBuilder htmlBuild = new StringBuilder();
		StringBuilder dateBuild = new StringBuilder();
		StringBuilder linkBuild = new StringBuilder();
		StringBuilder msgBuild = new StringBuilder();
		StringBuilder attachBuild = new StringBuilder();

		String[] dateArray, linkArray, msgArray, attachArray;

		protected ArrayList<Notice> doInBackground(String... arg0) {

			ArrayList<Notice> tnpNoticeList = new ArrayList<Notice>();

			try {
				URI listGetURI = new URI(arg0[0]);

				HttpClient tnpClient = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(
						tnpClient.getParams(), 30000);

				HttpGet sendTNPRequest = new HttpGet(listGetURI);
				HttpResponse getTNPList = tnpClient.execute(sendTNPRequest);

				if (getTNPList != null) {
					BufferedReader tnpClientBufferRead = new BufferedReader(
							new InputStreamReader(getTNPList.getEntity()
									.getContent(), "windows-1252"));

					String readHTMLline;

					while ((readHTMLline = tnpClientBufferRead.readLine()) != null) {
						htmlBuild.append(readHTMLline + "\n");
					}

					Document htmlBuildDoc = Jsoup.parse(htmlBuild.toString(),
							"windows-1252");
					Elements tnpListElements = htmlBuildDoc
							.getElementsByTag("td");
					Elements tnpLinkElements = htmlBuildDoc
							.getElementsByTag("a");

					for (Element eachEle : tnpListElements) {
						String widthAttribute = eachEle.attr("width");
						String widtAttribute = eachEle.attr("widt");

						if (widthAttribute.contentEquals("5%")
								|| widtAttribute.contentEquals("5%")) {

							if (eachEle.text().contentEquals("A")) {

								attachBuild.append("A" + "\n");
							} else {
								attachBuild.append(" " + "\n");
							}

						} else if (widthAttribute.contentEquals("25%")) {

							dateBuild.append(eachEle.text() + "\n");

						} else if (widthAttribute.contentEquals("70%")) {
							msgBuild.append(eachEle.text() + "\n");
						}

					}

					for (Element eachEle : tnpLinkElements) {
						linkBuild.append(eachEle.attr("href").replace(
								"notice.php?sr_no=", "")
								+ "\n");
					}

					dateArray = dateBuild.toString().split("\n");
					attachArray = attachBuild.toString().split("\n");
					linkArray = linkBuild.toString().split("\n");
					msgArray = msgBuild.toString().split("\n");

					int dateArrayLength = dateArray.length;
					int msgArrayLength = msgArray.length;

					if (msgArrayLength != dateArrayLength) {
						TNPListView.this.finish();
					} else {

						for (int i = 0; i < dateArrayLength; i++) {
							Notice notice = new Notice();
							notice.postNumber = linkArray[i];
							notice.anyAttachments = attachArray[i];
							notice.postDateTime = dateArray[i];
							notice.postMessage = msgArray[i];
							tnpNoticeList.add(notice);
							// noticeList.add(notice);
						}
					}

				}

			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// }
			return tnpNoticeList;

		}

		protected void onPostExecute(ArrayList<Notice> results) {

			if (!isCancelled() && tnpListadapter.isFilteringComplete) {

				y++;
				for (int i = 0; i < results.size(); i++) {

					noticeList.add(results.get(i));
					tnpListadapter.notifyDataSetChanged();
					isDatasetModifying = false;

				}

				isLoadingOnScroll = false;

				if (y == 10) {
					Toast.makeText(TNPListView.this,
							"Displaying results upto page: " + pgNo,
							Toast.LENGTH_SHORT).show();
					isLoadingOnClick = false;
					tnpListadapter.getFilter().filter(searchQuery);

					displayLoading.dismiss();

					tnpListFooterLoading.findViewById(R.id.loading_progress)
							.setVisibility(View.INVISIBLE);
					tnpListFooterLoading.findViewById(R.id.footerListView)
							.setVisibility(View.VISIBLE);

				}

			} else {

				tnpListFooterLoading.findViewById(R.id.loading_progress)
						.setVisibility(View.INVISIBLE);
				tnpListFooterLoading.findViewById(R.id.footerListView)
						.setVisibility(View.VISIBLE);

			}

			Collections.sort(noticeList, new NoticeNumberComparator());

		}

	}

	public void startSession() {

		CheckInternetConnection checkConnection = new CheckInternetConnection();
		try {
			isConnectedtoInternet = checkConnection.execute().get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}

		if (isConnectedtoInternet) {

			displayLoading = new ProgressDialog(TNPListView.this);
			displayLoading.setTitle("Loading");
			displayLoading.setMessage("Please wait...");
			displayLoading.show();

			pgNo = 1;
			noticePgURIString = defaultTNPPageLink + "index.php?page=" + pgNo;

			TNPListGetter getTNPpgList = new TNPListGetter();
			getTNPpgList.execute(noticePgURIString);

			tnpListFooterLoading = ((LayoutInflater) TNPListView.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.tnplistview_footer_viewmore, null, false);

			tnpListFooterLoading.findViewById(R.id.loading_progress)
					.setVisibility(View.INVISIBLE);

			tnpListViewPage.setOnScrollListener(new ContinousScrollListener());
			tnpListViewPage.addFooterView(tnpListFooterLoading);

		} else {

			AlertDialog.Builder noConnectionAlertb = new AlertDialog.Builder(
					TNPListView.this);
			noConnectionAlertb
					.setMessage(
							"No Connection Available. Please turn on your WiFi Module (IIT-KGP)")
					.setCancelable(false)
					.setNegativeButton("Close app",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									TNPListView.this.finish();
								}
							})
					.setPositiveButton("Try Again",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
									startSession();
								}
							});

			AlertDialog noConnectionAlert = noConnectionAlertb.create();
			noConnectionAlert.show();

		}
	}

	public void setNotificationAlarm() {

		Intent startNotifService = new Intent(TNPListView.this,
				TnPNotifier.class);

		// System.out.println("Notification Alarm has been set");
		AlarmManager scheduler = (AlarmManager) getSystemService(ALARM_SERVICE);
		PendingIntent scheduledNotifier = PendingIntent.getService(
				TNPListView.this, 0, startNotifService,
				PendingIntent.FLAG_UPDATE_CURRENT);
		scheduler.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				30000, 30000, scheduledNotifier);

	}
	
	// Module to set Bookmarks

	public boolean setBookmark(String arg01) {
		
		return true;
	}

}
