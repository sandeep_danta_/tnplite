package com.limelitesolutions.tnplite;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class TnPNotifier extends Service {

	public IBinder onBind(Intent arg0) {
		return null;
	}

	private void intentHandler(Intent arg0) {

		NotficationHandler notifyServiceThread = new NotficationHandler();
		notifyServiceThread.execute();

	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		System.out.println("SERVICE STARTED");
		intentHandler(intent);
		return START_STICKY;

	}

	private class NotficationHandler extends AsyncTask<Void, Void, Void> {

		StringBuilder htmlBuild = new StringBuilder();
		StringBuilder dateBuild = new StringBuilder();
		StringBuilder linkBuild = new StringBuilder();
		StringBuilder msgBuild = new StringBuilder();
		StringBuilder attachBuild = new StringBuilder();

		String[] dateArray, linkArray, msgArray, attachArray;

		ArrayList<Notice> tnpNoticeList = new ArrayList<Notice>();

		protected Void doInBackground(Void... arg0) {

			try {

				URI listGetURI = new URI("http://tp.iitkgp.ernet.in/notice/");

				HttpClient tnpClient = new DefaultHttpClient();
				HttpConnectionParams.setConnectionTimeout(
						tnpClient.getParams(), 10000);

				HttpGet sendTNPRequest = new HttpGet(listGetURI);
				HttpResponse getTNPList = tnpClient.execute(sendTNPRequest);

				if (getTNPList != null) {
					BufferedReader tnpClientBufferRead = new BufferedReader(
							new InputStreamReader(getTNPList.getEntity()
									.getContent()));

					String readHTMLline;

					while ((readHTMLline = tnpClientBufferRead.readLine()) != null) {
						htmlBuild.append(readHTMLline + "\n");
					}

					Document htmlBuildDoc = Jsoup.parse(htmlBuild.toString());
					Elements tnpListElements = htmlBuildDoc
							.getElementsByTag("td");
					Elements tnpLinkElements = htmlBuildDoc
							.getElementsByTag("a");

					for (Element eachEle : tnpListElements) {
						String widthAttribute = eachEle.attr("width");
						String widtAttribute = eachEle.attr("widt");

						if (widthAttribute.contentEquals("5%")
								|| widtAttribute.contentEquals("5%")) {

							if (eachEle.text().contentEquals("A")) {

								attachBuild.append("A" + "\n");
							} else {
								attachBuild.append(" " + "\n");
							}

						} else if (widthAttribute.contentEquals("25%")) {

							dateBuild.append(eachEle.text() + "\n");

						} else if (widthAttribute.contentEquals("70%")) {
							msgBuild.append(eachEle.text() + "\n");
						}

					}

					for (Element eachEle : tnpLinkElements) {
						linkBuild.append(eachEle.attr("href").replace(
								"notice.php?sr_no=", "")
								+ "\n");
					}

					dateArray = dateBuild.toString().split("\n");
					attachArray = attachBuild.toString().split("\n");
					linkArray = linkBuild.toString().split("\n");
					msgArray = msgBuild.toString().split("\n");

					int dateArrayLength = dateArray.length;
					int msgArrayLength = msgArray.length;

					if (msgArrayLength != dateArrayLength) {
						stopSelf();
					} else {

						for (int i = 0; i < dateArrayLength; i++) {
							Notice notice = new Notice();
							notice.postNumber = linkArray[i];
							notice.anyAttachments = attachArray[i];
							notice.postDateTime = dateArray[i];
							notice.postMessage = msgArray[i];
							tnpNoticeList.add(notice);
						}
					}

					InputStream fileNameInStream;
					fileNameInStream = openFileInput("lastNotif");
					BufferedReader buff = new BufferedReader(
							new InputStreamReader(fileNameInStream));
					StringBuilder sb = new StringBuilder();
					String readline;

					while ((readline = buff.readLine()) != null) {
						sb.append(readline);

					}
					fileNameInStream.close();

					String storedDate = sb.toString();
					if (!storedDate.contentEquals("") || storedDate != null) {

						int latestPost = Integer.parseInt(storedDate);
						writeToFile(tnpNoticeList, "lastNotif");

						int lastInCurrentList = Integer.parseInt(tnpNoticeList
								.get(tnpNoticeList.size() - 1).postNumber);

						System.out
								.println("Notification service is functioning");
						System.out.println(storedDate);
						System.out.println(lastInCurrentList);

						if (latestPost > lastInCurrentList) {

							for (int i = 0; i < tnpNoticeList.size(); i++) {

								int iterateInCurrentList = Integer
										.parseInt(tnpNoticeList.get(i).postNumber);
								if (iterateInCurrentList == latestPost) {

									int latestIndex = i;

									Intent resultIntent = new Intent(
											TnPNotifier.this, TNPListView.class);
									PendingIntent resultPendingIntent = PendingIntent
											.getActivity(
													TnPNotifier.this,
													0,
													resultIntent,
													PendingIntent.FLAG_UPDATE_CURRENT);

									int mID = 2;

									Uri defaultNotifSound = RingtoneManager
											.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

									NotificationCompat.Builder tnpNotifBuilder = new NotificationCompat.Builder(
											TnPNotifier.this)
											.setSmallIcon(
													R.drawable.ic_launcher)
											.setSound(defaultNotifSound)
											.setContentTitle(
													"Latest TnP Updates")
											.setContentIntent(
													resultPendingIntent);

									int notifCount = 0;

									NotificationManager tnpNotifManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
									for (int j = latestIndex - 1; j >= 0; j--) {

										notifCount++;

										if (notifCount == 1) {

											tnpNotifBuilder
													.setContentText(tnpNoticeList
															.get(j).postMessage);

											Notification updateNotif = tnpNotifBuilder
													.getNotification();
											updateNotif.flags |= Notification.FLAG_AUTO_CANCEL;

											tnpNotifManager.notify(mID,
													updateNotif);
										} else if (notifCount > 1) {

											tnpNotifBuilder
													.setContentText("You have "
															+ notifCount
															+ " NEW messages");

											Notification updateNotif = tnpNotifBuilder
													.getNotification();
											updateNotif.flags |= Notification.FLAG_AUTO_CANCEL;

											tnpNotifManager.notify(mID,
													updateNotif);

										}
									}

								}
								// stopSelf();

							}
						}
					}

				}

			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			return null;

		}

	}

	public void writeToFile(ArrayList<Notice> notice, String filename) {

		try {

			FileOutputStream fileNameStream;
			fileNameStream = openFileOutput(filename, Context.MODE_PRIVATE);

			fileNameStream.write(notice.get(0).postNumber.getBytes());
			fileNameStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
